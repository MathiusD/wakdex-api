from flask import Flask
from flask_restful import Resource, Api
from apispec import APISpec
from marshmallow import Schema, fields
from apispec.ext.marshmallow import MarshmallowPlugin
from flask_apispec.extension import FlaskApiSpec
from flask_apispec.views import MethodResource
from flask_apispec import marshal_with, doc

app = Flask(__name__)
api = Api(app)

app.config.update({
    'APISPEC_SPEC': APISpec(
        title='WakDex Api',
        version='0',
        plugins=[MarshmallowPlugin()],
        openapi_version='2.0.0'
    ),
    'APISPEC_SWAGGER_URL': '/docs/api',
    'APISPEC_SWAGGER_UI_URL': '/docs'
})
docs = FlaskApiSpec(app)


class MonsterResponseSchema(Schema):
    name = fields.Str(default='Example Name')
    description = fields.Str(default='Example Description')

class MonsterAPI(MethodResource, Resource):
    @doc(description='For get Monster Informations')
    @marshal_with(MonsterResponseSchema)
    def get(self, id:int):
        return {
            "name":"Bouchtrou l'esseulé",
            "description":"Archimonstre pour faire bouche-trou"
        }


api.add_resource(MonsterAPI, '/monster/<int:id>')
docs.register(MonsterAPI)