default-target: launch

build:
	@pip install -r requirements.txt

start:
	@flask run -p 8000 -h 0.0.0.0

launch: build start