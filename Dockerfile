FROM python

COPY . /app

WORKDIR /app

RUN make build

EXPOSE 8000

CMD ["make", "start"]
